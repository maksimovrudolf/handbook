import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import Init from './Init';
import { store } from './redux';

export default function App() {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <Init />
      </Provider>
    </NavigationContainer>
  );
}
