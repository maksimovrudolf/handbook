import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { PRIMARY_COLOR } from './config/colors';
import SignInScreen from './screens/SignInScreen';

const Stack = createStackNavigator();

function Auth() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: PRIMARY_COLOR,
        },
        headerTitleStyle: {
          fontSize: 16,
        },
        headerTintColor: '#fff',
      }}
      initialRouteName="SignIn">
      <Stack.Screen
        name="SignIn"
        component={SignInScreen}
        options={{ title: 'Вход в личный кабинет' }}
      />
    </Stack.Navigator>
  );
}

export default Auth;
