import AsyncStorage from '@react-native-async-storage/async-storage';
import { SIGN_IN, SIGN_OUT } from '../types';

export const signIn = (login, isSave) => async (disptach) => {
  if (isSave) {
    await AsyncStorage.setItem('login', login);
  }

  disptach({
    type: SIGN_IN,
    payload: login,
  });
};

export const signOut = () => async (dispach) => {
  await AsyncStorage.setItem('login', '');

  dispach({
    type: SIGN_OUT,
  });
};
