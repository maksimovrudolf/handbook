const initialState = {
  items: [
    {
      id: 1,
      title: 'HTML5',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aliquam beatae deleniti doloremque illum ipsum iure molestias, nihil nobis, nulla tenetur, totam ullam! Enim libero possimus quaerat sed suscipit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aliquam beatae deleniti doloremque illum ipsum iure molestias, nihil nobis, nulla tenetur, totam ullam!',
    },
    {
      id: 2,
      title: 'CSS3',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam beatae eligendi enim fugiat ipsa, iure iusto, laboriosam maiores molestiae nisi nobis optio provident quis quod quos recusandae sit temporibus voluptatibus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam beatae eligendi enim fugiat ipsa, iure iusto, laboriosam maiores molestiae nisi nobis optio provident!',
    },
    {
      id: 3,
      title: 'JavaScript',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At commodi cumque cupiditate dolore minima nam nisi nobis numquam officia placeat praesentium, sit voluptate voluptatibus! Debitis dolorum omnis rem suscipit tenetur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. At commodi cumque cupiditate dolore minima nam nisi nobis numquam officia placeat praesentium, sit voluptate.',
    },
    {
      id: 4,
      title: 'React.js',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aliquam beatae deleniti doloremque illum ipsum iure molestias, nihil nobis, nulla tenetur, totam ullam! Enim libero possimus quaerat sed suscipit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aliquam beatae deleniti doloremque illum ipsum iure molestias, nihil nobis, nulla tenetur, totam ullam.',
    },
    {
      id: 5,
      title: 'React Native',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, cum delectus doloribus earum eligendi et excepturi exercitationem iste laborum mollitia nostrum nulla, quisquam, quod sint suscipit voluptatem voluptates! Ut, vitae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, cum delectus doloribus earum eligendi et excepturi exercitationem iste laborum mollitia nostrum.',
    },
    {
      id: 6,
      title: 'Node.js',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur explicabo illo minima neque voluptatem! Architecto autem doloribus earum eligendi eveniet, excepturi facere inventore iusto magni natus numquam obcaecati reiciendis repellendus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur explicabo illo minima neque voluptatem! Architecto autem doloribus earum eligendi eveniet!',
    },
    {
      id: 7,
      title: 'Redux',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam aperiam, assumenda corporis dolore dolorum eos et exercitationem illo iusto magni maiores numquam placeat qui saepe similique sint tenetur totam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam aperiam, assumenda corporis dolore dolorum eos et exercitationem illo iusto magni maiores numquam placeat qui saepe similique?',
    },
    {
      id: 8,
      title: 'Express.js',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet beatae earum mollitia nesciunt nostrum nulla obcaecati porro provident quo soluta. Ab, eligendi expedita modi nam necessitatibus nesciunt quas quisquam temporibus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet beatae earum mollitia nesciunt nostrum nulla obcaecati porro provident quo soluta. Ab, eligendi expedita modi nam necessitatibus!',
    },
    {
      id: 9,
      title: 'Nest.js',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aliquam beatae deleniti doloremque illum ipsum iure molestias, nihil nobis, nulla tenetur, totam ullam! Enim libero possimus quaerat sed suscipit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aliquam beatae deleniti doloremque illum ipsum iure molestias, nihil nobis, nulla tenetur, totam ullam! Enim libero possimus quaerat.',
    },
    {
      id: 10,
      title: 'TypeScript',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aliquam beatae deleniti doloremque illum ipsum iure molestias, nihil nobis, nulla tenetur, totam ullam! Enim libero possimus quaerat sed suscipit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aliquam beatae deleniti doloremque illum ipsum iure molestias, nihil nobis, nulla tenetur, totam ullam! Enim libero possimus quaerat.',
    },
  ],
};

export default (state = initialState) => {
  return state;
};
