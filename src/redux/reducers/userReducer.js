import { SIGN_IN, SIGN_OUT } from '../types';

const initialState = {
  login: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN:
      return {
        ...state,
        login: action.payload,
      };
    case SIGN_OUT:
      return {
        ...state,
        login: null,
      };
    default:
      return state;
  }
};
