import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ListScreen from './screens/ListScreen';
import DetailScreen from './screens/DetailScreen';
import HeaderRight from './components/HeaderRight';
import { PRIMARY_COLOR } from './config/colors';

const Stack = createStackNavigator();

function Main() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: PRIMARY_COLOR,
        },
        headerTitleStyle: {
          fontSize: 16,
          fontWeight: '400',
        },
        headerTintColor: '#fff',
        headerRight: () => <HeaderRight />,
      }}
      initialRouteName="List">
      <Stack.Screen
        name="List"
        component={ListScreen}
        options={{ title: 'Список' }}
      />
      <Stack.Screen
        name="Detail"
        component={DetailScreen}
        options={{ title: ' ' }}
      />
    </Stack.Navigator>
  );
}

export default Main;
