import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { signIn } from './redux/actions/userAction';
import Auth from './Auth';
import Main from './Main';

function Init({ user, signInDispatch }) {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const bootstrapAsync = async () => {
      const login = await AsyncStorage.getItem('login');
      await signInDispatch(login ?? '');
      setIsLoading(false);
    };

    setTimeout(bootstrapAsync, 0);
  }, []);

  if (isLoading) return <></>;

  return user.login ? <Main /> : <Auth />;
}

const matStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = {
  signInDispatch: signIn,
};

export default connect(matStateToProps, mapDispatchToProps)(Init);
