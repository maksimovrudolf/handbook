import { Text, View, ScrollView } from 'react-native';
import React, { useLayoutEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import styles from './styles';
import Nav from '../../components/Nav';

function Index({ route, list }) {
  const { itemIndex } = route.params;
  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions({
      title: list.items[itemIndex].title,
    });
  });

  return (
    <View style={styles.root}>
      <ScrollView style={styles.content}>
        <View style={styles.info}>
          <Text style={styles.title}>{list.items[itemIndex].title}</Text>
          <Text style={styles.text}>{list.items[itemIndex].description}</Text>
          <Text style={styles.text}>{list.items[itemIndex].description}</Text>
          <Text style={styles.text}>{list.items[itemIndex].description}</Text>
        </View>
      </ScrollView>
      <Nav />
    </View>
  );
}

const matStateToProps = (state) => ({
  list: state.list,
});

export default connect(matStateToProps)(Index);
