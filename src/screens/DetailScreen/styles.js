import { StyleSheet } from 'react-native';
import { BACKGROUND_COLOR } from '../../config/colors';

export default StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
  },
  content: {
    flex: 1,
  },
  info: {
    padding: 20,
  },
  title: {
    fontSize: 16,
    color: '#000',
    marginBottom: 16,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 16,
    color: '#111',
    lineHeight: 24,
  },
});
