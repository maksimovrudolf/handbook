import { StyleSheet } from 'react-native';
import { BACKGROUND_COLOR, PRIMARY_COLOR } from '../../config/colors';

export default StyleSheet.create({
  wrapper: {
    flexGrow: 1,
    backgroundColor: BACKGROUND_COLOR,
  },
  container: {
    flex: 1,
  },
  inner: {
    flex: 1,
    padding: 20,
    justifyContent: 'center',
  },
  title: {
    color: '#c90f10',
    textAlign: 'center',
    fontSize: 40,
    fontWeight: 'bold',
    marginBottom: 24,
  },
  text: {
    color: '#959595',
    textAlign: 'center',
    marginBottom: 16,
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#d4d4d4',
    paddingHorizontal: 16,
    marginBottom: 10,
    fontSize: 14,
    color: '#000',
    height: 40,
  },
  buttonWrapper: {
    marginTop: 16,
    paddingHorizontal: 40,
  },
  button: {
    backgroundColor: PRIMARY_COLOR,
    color: '#fff',
    textAlign: 'center',
    fontSize: 14,
    padding: 10,
    borderRadius: 5,
    overflow: 'hidden',
  },
});
