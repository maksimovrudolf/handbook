import React from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { signIn } from '../../redux/actions/userAction';
import styles from './styles';

function Index({ signInDispatch }) {
  const [login, setLogin] = React.useState('');
  const [password, setPassword] = React.useState('');

  async function handleSubmit() {
    if (login && password) {
      await signInDispatch(login, true);
    } else {
      Alert.alert('Заполните все поля');
    }
  }

  return (
    <ScrollView contentContainerStyle={styles.wrapper}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.container}
      >
        <View style={styles.inner}>
          <Text style={styles.title}>Вход</Text>
          <Text style={styles.text}>
            Справочник программиста — общие сведения различных языков
            программирования.
          </Text>
          <TextInput
            placeholder="Логин"
            style={styles.input}
            onChangeText={setLogin}
          />
          <TextInput
            placeholder="Пароль"
            secureTextEntry
            style={styles.input}
            onChangeText={setPassword}
          />
          <View style={styles.buttonWrapper}>
            <TouchableOpacity onPress={handleSubmit}>
              <Text style={styles.button}>Войти</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
}

const mapDispatchToProps = {
  signInDispatch: signIn,
};

export default connect(null, mapDispatchToProps)(Index);
