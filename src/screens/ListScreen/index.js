import { View, ScrollView } from 'react-native';
import React from 'react';
import { connect } from 'react-redux';
import ListItem from '../../components/ListItem';
import styles from './styles';

function Index({ list }) {
  return (
    <ScrollView>
      <View style={styles.root}>
        {list.items.map((item, index) => (
          <ListItem key={item.id} title={item.title} itemIndex={index} />
        ))}
      </View>
    </ScrollView>
  );
}

const matStateToProps = (state) => ({
  list: state.list,
});

export default connect(matStateToProps)(Index);
