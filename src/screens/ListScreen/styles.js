import { StyleSheet } from 'react-native';
import { BACKGROUND_COLOR } from '../../config/colors';

export default StyleSheet.create({
  root: {
    flex: 1,
    padding: 8,
    backgroundColor: BACKGROUND_COLOR,
  },
});
