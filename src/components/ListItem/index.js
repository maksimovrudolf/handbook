import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';

function ListItem({ title, itemIndex }) {
  const navigation = useNavigation();

  function onPress() {
    navigation.navigate('Detail', { itemIndex });
  }

  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.root}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

export default ListItem;
