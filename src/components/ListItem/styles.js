import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  root: {
    borderWidth: 1,
    borderColor: '#d4d4d4',
    padding: 16,
    marginVertical: 8,
    fontSize: 14,
  },
});
