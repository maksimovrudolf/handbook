import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 16,
  },
  login: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '400',
  },
  icon: {
    width: 20,
    height: 20,
    marginLeft: 8,
  },
});
