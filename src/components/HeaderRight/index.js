import React from 'react';
import { Text, View, Image } from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';

function HeaderRight({ user }) {
  return (
    <View style={styles.root}>
      <Text style={styles.login}>{user.login}</Text>
      <Image source={require('../../icons/person.png')} style={styles.icon} />
    </View>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(HeaderRight);
