import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';
import { signOut } from '../../redux/actions/userAction';

function Nav({ signOutDispatch }) {
  const navigation = useNavigation();

  function handleGoBack() {
    navigation.goBack();
  }

  async function handleSignOut() {
    await signOutDispatch();
  }

  return (
    <View style={styles.root}>
      <TouchableOpacity style={[styles.button, styles.lightButton]} onPress={handleGoBack}>
        <Text style={[styles.buttonText, styles.lightButtonText]}>Назад</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={handleSignOut}>
        <Text style={styles.buttonText}>Выйти из аккаунта</Text>
      </TouchableOpacity>
    </View>
  );
}

const mapDispatchToProps = {
  signOutDispatch: signOut,
};

export default connect(null, mapDispatchToProps)(Nav);
