import { StyleSheet } from 'react-native';
import { PRIMARY_COLOR } from '../../config/colors';

export default StyleSheet.create({
  root: {
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    padding: 10,
    backgroundColor: PRIMARY_COLOR,
    borderWidth: 2,
    borderColor: PRIMARY_COLOR,
  },
  lightButton: {
    backgroundColor: '#fff',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
  },
  lightButtonText: {
    color: PRIMARY_COLOR,
  },
});
